#include "data.h"

void femdg_done(p4est_t * p4est)
{
    int mpiret;

    p4est_connectivity_destroy (p4est->connectivity);
    p4est_destroy(p4est);

    sc_finalize ();

    mpiret = sc_MPI_Finalize ();
    SC_CHECK_MPI (mpiret);

}
