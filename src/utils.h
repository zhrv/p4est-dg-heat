//
// Created by appmath on 10.06.17.
//

#ifndef P4EST_DG_HEAT_UTILS_H
#define P4EST_DG_HEAT_UTILS_H
#include "data.h"

extern void femdg_get_midpoint (p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * q, double xyz[3]);
extern double get_scal_prod (double v1[3],double v2[3]);
extern void femdg_calc_quad_mass_matr(p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * q);
extern void femdg_get_phi (double px,double py, double phi[3], p4est_quadrant_t * q,p4est_topidx_t which_tree,p4est_t * p4est);
extern void femdg_get_phi_dx (double xx,double yy, double phi[3], p4est_quadrant_t * q,p4est_topidx_t which_tree,p4est_t * p4est);
extern void femdg_get_phi_dy (double xx,double yy, double phi[3], p4est_quadrant_t * q,p4est_topidx_t which_tree,p4est_t * p4est);

extern double femdg_get_field_u(double x, double y, p4est_quadrant_t * q, p4est_topidx_t which_tree, p4est_t * p4est);
extern double femdg_get_field_qx(double x, double y, p4est_quadrant_t * q, p4est_topidx_t which_tree, p4est_t * p4est);
extern double femdg_get_field_qy(double x, double y, p4est_quadrant_t * q, p4est_topidx_t which_tree, p4est_t * p4est);

extern void femdg_calc_quad_gp(p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *q);



#endif //P4EST_DG_HEAT_UTILS_H
