//
// Created by appmath on 10.06.17.
//

#ifndef P4EST_DG_HEAT_TIMESTEP_H
#define P4EST_DG_HEAT_TIMESTEP_H

#include "data.h"

void femdg_timestep (p4est_t * p4est, double time);


#endif //P4EST_DG_HEAT_TIMESTEP_H
