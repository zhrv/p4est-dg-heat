#ifndef P4EST_DG_HEAT_DONE_H
#define P4EST_DG_HEAT_DONE_H
#include "data.h"

extern void femdg_done(p4est_t * p4est);

#endif //P4EST_DG_HEAT_DONE_H
