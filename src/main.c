#include "init.h"
#include "done.h"
#include "timestep.h"



int main(int argc, char **argv)
{
    p4est_t            *p4est;

    p4est = femdg_init(&argc, &argv);
    femdg_timestep(p4est, 1000.0);
    femdg_done(p4est);

    return 0;
}
