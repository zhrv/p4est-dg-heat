//
// Created by appmath on 10.06.17.
//

#include "data.h"


p4est_connectivity_t* femdg_conn_create()
{
    p4est_connectivity_t* conn;
#ifndef P4_TO_P8
    /*
    const p4est_topidx_t num_vertices = 22;
    const p4est_topidx_t num_trees = 10;
    const p4est_topidx_t num_corners = 0;
    const p4est_topidx_t tree_to_vertex[10 * 4] = {
            0, 1, 2, 3,
            2, 3, 4, 5,
            4, 5, 6, 7,
            6, 7, 8, 9,
            8, 9, 10, 11,
            10, 11, 12, 13,
            12, 13, 14, 15,
            14, 15, 16, 17,
            16, 17, 18, 19,
            18, 19, 20, 21,
    };
    const p4est_topidx_t tree_to_tree[10 * 4] = {
            0, 0, 0, 1,
            1, 1, 0, 2,
            2, 2, 1, 3,
            3, 3, 2, 4,
            4, 4, 3, 5,
            5, 5, 4, 6,
            6, 6, 5, 7,
            7, 7, 6, 8,
            8, 8, 7, 9,
            9, 9, 8, 9,
    };
    const int8_t        tree_to_face[10 * 4] = {
            0, 1, 2, 2,
            0, 1, 3, 2,
            0, 1, 3, 2,
            0, 1, 3, 2,
            0, 1, 3, 2,
            0, 1, 3, 2,
            0, 1, 3, 2,
            0, 1, 3, 2,
            0, 1, 3, 2,
            0, 1, 3, 3,
    };
    const p4est_topidx_t tree_to_corner[6 * 4] = {
            -1, -1, 0, 0,
            0, -1, -1, -1,
            0, 0, -1, 0,
            0, -1, 0, -1,
    };
    const p4est_topidx_t ctt_offset[1] = {
            0,
    };
    const p4est_topidx_t corner_to_tree[6] = {
            0, 1, 2, 3, 4, 5,
    };
    const int8_t        corner_to_corner[6] = {
            0, 0, 2, 3, 1, 3,
    };
    const double        pi = 4.0 * atan (1.0);
    const double        r1 = 1.;
    const double        r2 = 1.5;
    double              vertices[22 * 3];
    int                 i;


    for (i = 0; i < 11; i++) {
        vertices[(i*2+0) * 3 + 0] = 0;
        vertices[(i*2+0) * 3 + 1] = -0.1+0.04*i;
        vertices[(i*2+0) * 3 + 2] = 0;

        vertices[(i*2+1) * 3 + 0] = 0.04;
        vertices[(i*2+1) * 3 + 1] = -0.1+0.04*i;
        vertices[(i*2+1) * 3 + 2] = 0;
    }



    conn = p4est_connectivity_new_copy (num_vertices, num_trees, num_corners,
                                        vertices, tree_to_vertex,
                                        tree_to_tree, tree_to_face,
                                        NULL, ctt_offset,
                                        NULL, NULL);
                                        */
    conn = p4est_connectivity_new_unitsquare();
#else
    conn = p8est_connectivity_new_periodic ();
#endif

    return conn;
}
