//
// Created by appmath on 10.06.17.
//
#ifndef P4EST_DG_HEAT_INTEGRALS_H
#define P4EST_DG_HEAT_INTEGRALS_H

#include "data.h"
#include "utils.h"


extern void femdg_zero_integrals(p4est_iter_volume_info_t * info, void *user_data);
extern void femdg_calc_double_integral(p4est_iter_volume_info_t * info, void *user_data);
extern void femdg_calc_flux(p4est_iter_face_info_t * info, void *user_data);
extern void femdg_calc_lin_integral(p4est_iter_volume_info_t * info, void *user_data);

extern void femdg_calc_double_integral_q(p4est_iter_volume_info_t * info, void *user_data);
extern void femdg_calc_flux_q(p4est_iter_face_info_t * info, void *user_data);
extern void femdg_calc_lin_integral_q(p4est_iter_volume_info_t * info, void *user_data);



#endif //P4EST_DG_HEAT_INTEGRALS_H
