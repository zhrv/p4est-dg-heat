//
// Created by appmath on 10.06.17.
//
#include "data.h"
#include "utils.h"


double femdg_error_sqr_estimate (p4est_quadrant_t * q)
{
    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;
    int                 i;
    double              diff2;
    double              du[2];
    double              h = ROOT_LEN *
                            (double) P4EST_QUADRANT_LEN (q->level) / (double) P4EST_ROOT_LEN;
//    double              vol;

    for (i = 0; i < P4EST_DIM; i++) {
        du[i] = data->u[i+1];
    }

//    vol = h * h;

    diff2 = 0.;
    /* use the approximate derivative to estimate the L2 error */
    for (i = 0; i < P4EST_DIM; i++) {
        diff2 += du[i] * du[i] * (1. / 12.) * h * h;// * vol;
    }

    return diff2;
}

int femdg_refine_err_estimate (p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * q)
{

    femdg_ctx_t        *ctx = (femdg_ctx_t *) p4est->user_pointer;
    double              global_err = ctx->max_err;
    double              global_err2 = global_err * global_err;
    double              h = ROOT_LEN *
                            (double) P4EST_QUADRANT_LEN (q->level) / (double) P4EST_ROOT_LEN;
    double              vol, err2;


    vol = h * h;

    err2 = femdg_error_sqr_estimate (q);
    if (err2 > global_err2 * vol) {
        return 1;
    }
    else {
        return 0;
    }



}

int femdg_refine_init_err_estimate (p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * q)
{
    femdg_ctx_t        *ctx = (femdg_ctx_t *) p4est->user_pointer;
    double              global_err = ctx->max_err;
    double              global_err2 = global_err * global_err;
    double              h = ROOT_LEN *
                            (double) P4EST_QUADRANT_LEN (q->level) / (double) P4EST_ROOT_LEN;
    double              vol, err2;

    if (q->level > 7) {
        return 0;
    }
    //return 1;

    vol = h * h;

    err2 = femdg_error_sqr_estimate (q);
    if (err2 > global_err2 * vol) {
        return 1;
    }
    else {
        return 0;
    }
}

int femdg_coarsen_init_err_estimate (p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * children[])
{
    return 0;
}

int femdg_coarsen_err_estimate (p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * children[])
{
    femdg_ctx_t           *ctx = (femdg_ctx_t *) p4est->user_pointer;
    double              global_err = ctx->max_err;
    double              global_err2 = global_err * global_err;
    double              h;
    femdg_data_t          *data;
    double              vol, err2, childerr2;
    double              parentu;
    double              diff;
    int                 i;

//    if (children[0]->level < 4) {
//        return 0;
//    }


    h =     ROOT_LEN *
            (double) P4EST_QUADRANT_LEN (children[0]->level) /
            (double) P4EST_ROOT_LEN;
    /* the quadrant's volume is also its volume fraction */
    vol = h * h;

    /* compute the average */
    parentu = 0.;
    for (i = 0; i < P4EST_CHILDREN; i++) {
        data = (femdg_data_t *) children[i]->p.user_data;
        parentu += data->u[0] / P4EST_CHILDREN;
    }

    err2 = 0.;
    for (i = 0; i < P4EST_CHILDREN; i++) {
        childerr2 = femdg_error_sqr_estimate (children[i]);

        if (childerr2 > global_err2 * vol) {
            return 0;
        }
        err2 += femdg_error_sqr_estimate (children[i]);
        diff = (parentu - data->u[0]) * (parentu - data->u[0]);
        err2 += diff * vol;
    }
    if (err2 < global_err2 * (vol * P4EST_CHILDREN)) {
        return 1;
    }
    else {
        return 0;
    }

//    double c[3];
//
//    for (i = 0; i < P4EST_CHILDREN; i++) {
//  femdg_get_midpoint(p4est,which_tree,children[i],c);
//        if (c[0]<0.5 && c[1]<0.5){ return 0;}
//        else{ return 1;}
//
//   }



}

void femdg_replace_quads (p4est_t * p4est, p4est_topidx_t which_tree,
                                 int num_outgoing, p4est_quadrant_t * outgoing[],
                                 int num_incoming, p4est_quadrant_t * incoming[])
{
    femdg_data_t       *parent_data, *child_data;
    int                 i/*, j*/;
//    double              h;
//    double              du_old, du_est;

    if (num_outgoing > 1) {
        /* this is coarsening */
        parent_data = (femdg_data_t *) incoming[0]->p.user_data;
        parent_data->u[0] = 0.;
        parent_data->u[1] = 0.;
        parent_data->u[2] = 0.;

        for (i = 0; i < P4EST_CHILDREN; i++) {
            child_data = (femdg_data_t *) outgoing[i]->p.user_data;
            parent_data->u[0] += child_data->u[0] / P4EST_CHILDREN;

        }

        femdg_calc_quad_gp(p4est,which_tree, incoming[0]);
        femdg_calc_quad_mass_matr(p4est, which_tree, incoming[0]);
    }
    else {

        /* this is refinement */
        parent_data = (femdg_data_t *) outgoing[0]->p.user_data;
//        h = ROOT_LEN *
//            (double) P4EST_QUADRANT_LEN (outgoing[0]->level) /
//            (double) P4EST_ROOT_LEN;

        for (i = 0; i < P4EST_CHILDREN; i++) {
            child_data = (femdg_data_t *) incoming[i]->p.user_data;
            child_data->u[0] = parent_data->u[0];
            child_data->u[1]=0;
            child_data->u[2]=0;

            femdg_calc_quad_gp(p4est,which_tree, incoming[i]);
            femdg_calc_quad_mass_matr(p4est, which_tree, incoming[i]);
        }
    }
}


/**
 * adapt
 */
void femdg_adapt(p4est_t * p4est)
{
    femdg_ctx_t        *ctx = (femdg_ctx_t *) p4est->user_pointer;
    int                 recursive = 0;
    int                 allowed_level = ctx->max_level;
    int                 callbackorphans = 0;

    p4est_refine_ext (p4est, recursive, allowed_level,
                      femdg_refine_err_estimate, NULL,
                      femdg_replace_quads);
    p4est_coarsen_ext (p4est, recursive, callbackorphans,
                       femdg_coarsen_err_estimate, NULL,
                       femdg_replace_quads);
    p4est_balance_ext (p4est, P4EST_CONNECT_FACE, NULL,
                       femdg_replace_quads);

}

