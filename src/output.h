//
// Created by appmath on 10.06.17.
//

#ifndef P4EST_DG_HEAT_OUTPUT_H
#define P4EST_DG_HEAT_OUTPUT_H

#include "data.h"

void femdg_write_solution (p4est_t * p4est, int timestep);

#endif //P4EST_DG_HEAT_OUTPUT_H
