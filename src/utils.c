//
// Created by appmath on 10.06.17.
//

#include "data.h"

void femdg_get_midpoint (p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * q, double xyz[3])
{
//    double              h = ROOT_LEN *
//                            (double) P4EST_QUADRANT_LEN (q->level) / (double) P4EST_ROOT_LEN;
    p4est_qcoord_t      half_length = P4EST_QUADRANT_LEN (q->level) / 2;

    p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                            q->x + half_length, q->y + half_length,
#ifdef P4_TO_P8
            q->z + half_length,
#endif
                            xyz);


}

double get_scal_prod (double v1[3],double v2[3]){
    double scal=0;
    for(int i=0;i<3;i++) scal+=v1[i]*v2[i];
    return scal;
}




/****************************
 *   BASIS FUNCTIONS
 ****************************/



//static void femdg_inverse_matr(double a[3][3], double am[3][3])
//{
//
//
//    double detA = a[0][0] * a[1][1] * a[2][2] + a[1][0] * a[2][1] * a[0][2] + a[0][1] * a[1][2] * a[2][0]
//                  - a[2][0] * a[1][1] * a[0][2] - a[1][0] * a[0][1] * a[2][2] - a[0][0] * a[2][1] * a[1][2];
//    //OutPut("detA = %25.16e\n", detA);
//    double m[3][3];
//    m[0][0] = a[1][1] * a[2][2] - a[2][1] * a[1][2];
//    m[0][1] = a[2][0] * a[1][2] - a[1][0] * a[2][2];
//    m[0][2] = a[1][0] * a[2][1] - a[2][0] * a[1][1];
//    m[1][0] = a[2][1] * a[0][2] - a[0][1] * a[2][2];
//    m[1][1] = a[0][0] * a[2][2] - a[2][0] * a[0][2];
//    m[1][2] = a[2][0] * a[0][1] - a[0][0] * a[2][1];
//    m[2][0] = a[0][1] * a[1][2] - a[1][1] * a[0][2];
//    m[2][1] = a[1][0] * a[0][2] - a[0][0] * a[1][2];
//    m[2][2] = a[0][0] * a[1][1] - a[1][0] * a[0][1];
//
//    am[0][0] = m[0][0] / detA;
//    am[0][1] = m[1][0] / detA;
//    am[0][2] = m[2][0] / detA;
//    am[1][0] = m[0][1] / detA;
//    am[1][1] = m[1][1] / detA;
//    am[1][2] = m[2][1] / detA;
//    am[2][0] = m[0][2] / detA;
//    am[2][1] = m[1][2] / detA;
//    am[2][2] = m[2][2] / detA;
//}

void femdg_calc_quad_mass_matr(p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * q){
    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;
    double              h = ROOT_LEN *
                            (double) P4EST_QUADRANT_LEN (q->level) / (double) P4EST_ROOT_LEN;
    double diag[3] = { 1.0/(h*h), 12.0/(h*h), 12.0/(h*h) };
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            data->invMassM[i][j] = (i == j ? diag[i] : 0.0);
        }
    }

}


void femdg_get_phi (double px,double py, double phi[3], p4est_quadrant_t * q,p4est_topidx_t which_tree,p4est_t * p4est){
    double              h = ROOT_LEN * (double) P4EST_QUADRANT_LEN (q->level) / (double) P4EST_ROOT_LEN;
//    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;
    double              midpoint[3];
    femdg_get_midpoint (p4est, which_tree, q, midpoint);

    phi[0]=1;
    phi[1]=(px-midpoint[0])/h;
    phi[2]=(py-midpoint[1])/h;

}


void femdg_get_phi_dx (double xx,double yy, double phi[3], p4est_quadrant_t * q,p4est_topidx_t which_tree,p4est_t * p4est){
    double              h = ROOT_LEN *
                            (double) P4EST_QUADRANT_LEN (q->level) / (double) P4EST_ROOT_LEN;
//    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;
    phi[0]=0.0;
    phi[1]=1.0/h;
    phi[2]=0.0;
}

void femdg_get_phi_dy (double xx,double yy, double phi[3], p4est_quadrant_t * q,p4est_topidx_t which_tree,p4est_t * p4est){
    double              h = ROOT_LEN * (double) P4EST_QUADRANT_LEN (q->level) / (double) P4EST_ROOT_LEN;
    phi[0]=0.0;
    phi[1]=0.0;
    phi[2]=1.0/h;
}

double femdg_get_field_u(double x, double y, p4est_quadrant_t * q, p4est_topidx_t which_tree, p4est_t * p4est) {
    double phi[3];
    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;

    femdg_get_phi(x, y, phi, q, which_tree, p4est);
    return get_scal_prod(phi, data->u);

}


double femdg_get_field_qx(double x, double y, p4est_quadrant_t * q, p4est_topidx_t which_tree, p4est_t * p4est) {
    double phi[3];
    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;

    femdg_get_phi(x, y, phi, q, which_tree, p4est);
    return get_scal_prod(phi, data->qx);

}


double femdg_get_field_qy(double x, double y, p4est_quadrant_t * q, p4est_topidx_t which_tree, p4est_t * p4est) {
    double phi[3];
    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;

    femdg_get_phi(x, y, phi, q, which_tree, p4est);
    return get_scal_prod(phi, data->qy);

}



void femdg_calc_quad_gp(p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *q)
{
    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;
    double              h = ROOT_LEN * (double) P4EST_QUADRANT_LEN (q->level) / (double) P4EST_ROOT_LEN;
    double              h2 = h/2.0;

//    p4est_qcoord_t half_length = P4EST_QUADRANT_LEN (q->level) / 2;

    double midpoint[3];
    femdg_get_midpoint(p4est, which_tree, q, midpoint);
    const double SQRT3 = 1.0 / sqrt(3.0);


    double ax = midpoint[0] - h2; //x-h/2
    double bx = midpoint[0] + h2;;//x+h/2
    double ay = midpoint[1] - h2;;//y-h/2
    double by = midpoint[1] + h2;;//y+h/2

    double hx = 0.5*(bx-ax);
    double hy = 0.5*(by-ay);
    double cx = 0.5*(bx+ax);
    double cy = 0.5*(by+ay);


    data->gpf[2][0][0]=cx-hx*SQRT3;
    data->gpf[2][0][1]=ay;

    data->gpf[2][1][0]=cx+hx*SQRT3;
    data->gpf[2][1][1]=ay;

    data->gpf[1][0][0]=bx;
    data->gpf[1][0][1]=cy-hy*SQRT3;

    data->gpf[1][1][0]=bx;
    data->gpf[1][1][1]=cy+hy*SQRT3;

    data->gpf[3][1][0]=cx+hx*SQRT3;
    data->gpf[3][1][1]=by;

    data->gpf[3][0][0]=cx-hx*SQRT3;
    data->gpf[3][0][1]=by;

    data->gpf[0][1][0]=ax;
    data->gpf[0][1][1]=cy+hy*SQRT3;

    data->gpf[0][0][0]=ax;
    data->gpf[0][0][1]=cy-hy*SQRT3;

    data->gJx=hx;
    data->gJy=hy;

    data->gp[0][0]=cx-hx*SQRT3; data->gp[0][1]=cy-hy*SQRT3;
    data->gp[1][0]=cx-hx*SQRT3; data->gp[1][1]=cy+hy*SQRT3;
    data->gp[2][0]=cx+hx*SQRT3; data->gp[2][1]=cy-hy*SQRT3;
    data->gp[3][0]=cx+hx*SQRT3; data->gp[3][1]=cy+hy*SQRT3;

}
