//
// Created by appmath on 10.06.17.
//

#ifndef P4EST_DG_HEAT_CONNECTIVITY_H
#define P4EST_DG_HEAT_CONNECTIVITY_H

#include "data.h"

p4est_connectivity_t* femdg_conn_create();

#endif //P4EST_DG_HEAT_CONNECTIVITY_H
