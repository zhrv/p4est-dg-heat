#include "data.h"
#include "utils.h"

int femdg_get_hanging (p4est_iter_face_side_t *side[2] ){
    if (side[0]->is_hanging) return 0;
    if (side[1]->is_hanging) return 1;
    return -1;
}


void femdg_zero_integrals(p4est_iter_volume_info_t * info, void *user_data){
    p4est_quadrant_t   *q = info->quad;
    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;

    memset(data->u_int,  0, 3*sizeof(double));
    memset(data->qx_int, 0, 3*sizeof(double));
    memset(data->qy_int, 0, 3*sizeof(double));
}




void femdg_calc_double_integral(p4est_iter_volume_info_t * info, void *user_data) {

    p4est_t            *p4est = info->p4est;
    p4est_quadrant_t   *q = info->quad;
    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;
    double              h = ROOT_LEN * (double) P4EST_QUADRANT_LEN (q->level) / (double) P4EST_ROOT_LEN;
    double              midpoint[3];
    double              vs[3]={0,0,0};
    double              phiDx[3], phiDy[3];

    femdg_get_midpoint (p4est, info->treeid, q, midpoint);

    for (int j = 0; j < 4; j++){
        femdg_get_phi_dx(data->gp[j][0], data->gp[j][1], phiDx, q, info->treeid, p4est);
        femdg_get_phi_dy(data->gp[j][0], data->gp[j][1], phiDy, q, info->treeid, p4est);
        double qx_phi=femdg_get_field_qx(data->gp[j][0], data->gp[j][1], q, info->treeid, p4est);
        double qy_phi=femdg_get_field_qy(data->gp[j][0], data->gp[j][1], q, info->treeid, p4est);
        for (int i = 0; i < 3;i++) {
            vs[i] += phiDx[i] * qx_phi + phiDy[i] * qy_phi;
        }
    }

    for (int i = 0; i < 3; i++) {
        data->u_int[i] -= vs[i] * (h*h)/4.0;
    }
}



void femdg_calc_flux(p4est_iter_face_info_t * info, void *user_data)
{
    p4est_quadrant_t   *q0[2],*q1;
    p4est_iter_face_side_t *side[2];
    sc_array_t         *sides = &(info->sides);
    int                 which_face;
    femdg_data_t       *udata, *hdata[2];
    p4est_t            *p4est = info->p4est;


    if (sides->elem_count != 2) {
        side[0] = p4est_iter_fside_array_index_int(sides, 0);
        q0[0] = side[0]->is.full.quad;
        hdata[0] = q0[0]->p.user_data;
        hdata[0]->is_h[side[0]->face] = 0;
        which_face = side[0]->face;
        for (int igp = 0; igp < 2; igp++) {
            double gp[2][2], gjx, gjy;
            gp[0][0] = hdata[0]->gpf[which_face][igp][0];
            gp[0][1] = hdata[0]->gpf[which_face][igp][1];
            gp[1][0] = gp[0][0];//udata->gpf[side[uside]->face][igp][0];
            gp[1][1] = gp[0][1];//udata->gpf[side[uside]->face][igp][1];
            gjx = hdata[0]->gJx;
            gjy = hdata[0]->gJy;
            double nx, ny;
            if (which_face / 2 == 0) {
                nx = (which_face % 2 ? 1.0 : -1.0);
                ny = 0;
            }
            else {
                nx = 0;
                ny = (which_face % 2 ? 1.0 : -1.0);
            }
            double qx_h = femdg_get_field_qx(gp[0][0], gp[0][1], q0[0], side[0]->treeid, p4est);
            double qy_h = femdg_get_field_qy(gp[0][0], gp[0][1], q0[0], side[0]->treeid, p4est);
            double qx_phi = 0.;//0.5 * (qx_h + 0);
            double qy_phi = 0.;//0.5 * (qy_h + 0);
            double phi_h[3], phi_u[3];
            femdg_get_phi(gp[0][0], gp[0][1], phi_h, q0[0], side[0]->treeid, p4est);
            for (int i = 0; i < 3; i++) {
                hdata[0]->u_int[i] += phi_h[i] * nx * qx_phi * gjy + phi_h[i] * ny * qy_phi * gjx;
            }
        }
    }
    else {
        side[0] = p4est_iter_fside_array_index_int(sides, 0);
        side[1] = p4est_iter_fside_array_index_int(sides, 1);
        int hside = femdg_get_hanging(side);
        int uside = (hside + 1) % 2;
        if (hside != -1) {
            q0[0] = side[hside]->is.hanging.quad[0];
            hdata[0] = q0[0]->p.user_data;
            hdata[0]->is_h[side[hside]->face] = 0;

            q0[1] = side[hside]->is.hanging.quad[1];
            hdata[1] = q0[1]->p.user_data;
            hdata[1]->is_h[side[hside]->face] = 0;

            which_face = side[hside]->face;
            q1 = side[(hside + 1) % 2]->is.full.quad;
            udata = q1->p.user_data;
            udata->is_h[side[uside]->face] = 1;


            for (int ih = 0; ih < 2; ih++) {
                for (int igp = 0; igp < 2; igp++) {
                    double gp[2], gjx, gjy;
                    gp[0] = hdata[ih]->gpf[which_face][igp][0];
                    gp[1] = hdata[ih]->gpf[which_face][igp][1];
                    gjx = hdata[ih]->gJx;
                    gjy = hdata[ih]->gJy;
                    double nx, ny;
                    if (which_face / 2 == 0) {
                        nx = (which_face % 2 ? 1.0 : -1.0);
                        ny = 0;
                    }
                    else {
                        nx = 0;
                        ny = (which_face % 2 ? 1.0 : -1.0);
                    }
                    double u_h = femdg_get_field_u(gp[0], gp[1], q0[ih], side[hside]->treeid, p4est);
                    double u_s = femdg_get_field_u(gp[0], gp[1], q1, side[uside]->treeid, p4est);
                    double u_phi = 0.5 * (u_h + u_s);
                    double phi_h[3], phi_u[3];
                    femdg_get_phi(gp[0], gp[1], phi_h, q0[ih], side[hside]->treeid, p4est);
                    femdg_get_phi(gp[0], gp[1], phi_u, q1, side[hside]->treeid, p4est);
                    for (int i = 0; i < 3; i++) {
                        hdata[ih]->qx_int[i] += phi_h[i] * nx * u_phi * gjy;
                        hdata[ih]->qy_int[i] += phi_h[i] * ny * u_phi * gjx;
                        udata->qx_int[i]     -= phi_u[i] * nx * u_phi * gjy;
                        udata->qy_int[i]     -= phi_u[i] * ny * u_phi * gjx;
                    }
                }
            }

        } else {
            int hside = 0;
            int uside = 1;
            q0[0] = side[hside]->is.full.quad;
            hdata[0] = q0[0]->p.user_data;
            hdata[0]->is_h[side[hside]->face] = 0;

            which_face = side[hside]->face;
            q1 = side[uside]->is.full.quad;
            udata = q1->p.user_data;
            udata->is_h[side[uside]->face] = 0;
            for (int igp = 0; igp < 2; igp++) {
                double gp[2][2], gjx, gjy;
                gp[0][0] = hdata[0]->gpf[which_face][igp][0];
                gp[0][1] = hdata[0]->gpf[which_face][igp][1];
                gp[1][0] = gp[0][0];//udata->gpf[side[uside]->face][igp][0];
                gp[1][1] = gp[0][1];//udata->gpf[side[uside]->face][igp][1];
                gjx = hdata[0]->gJx;
                gjy = hdata[0]->gJy;
                double nx, ny;
                if (which_face / 2 == 0) {
                    nx = (which_face % 2 ? 1.0 : -1.0);
                    ny = 0;
                }
                else {
                    nx = 0;
                    ny = (which_face % 2 ? 1.0 : -1.0);
                }
                double qx_h = femdg_get_field_qx(gp[0][0], gp[0][1], q0[0], side[hside]->treeid, p4est);
                double qy_h = femdg_get_field_qy(gp[0][0], gp[0][1], q0[0], side[hside]->treeid, p4est);
                double qx_s = femdg_get_field_qx(gp[1][0], gp[1][1], q1,    side[uside]->treeid, p4est);
                double qy_s = femdg_get_field_qy(gp[1][0], gp[1][1], q1,    side[uside]->treeid, p4est);
                double qx_phi = 0.5 * (qx_h + qx_s);
                double qy_phi = 0.5 * (qy_h + qy_s);
                double phi_h[3], phi_u[3];
                femdg_get_phi(gp[0][0], gp[0][1], phi_h, q0[0], side[hside]->treeid, p4est);
                femdg_get_phi(gp[1][0], gp[1][1], phi_u, q1,    side[uside]->treeid, p4est);
                for (int i = 0; i < 3; i++) {
                    hdata[0]->u_int[i] += (phi_h[i] * nx * qx_phi * gjy + phi_h[i] * ny * qy_phi * gjx);
                    udata->u_int[i]    -= (phi_u[i] * nx * qx_phi * gjy + phi_u[i] * ny * qy_phi * gjx);
                }
            }
        }
    }

}



