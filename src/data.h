//
// Created by appmath on 10.06.17.
//

#ifndef P4EST_DG_HEAT_DATA_H
#define P4EST_DG_HEAT_DATA_H

#include <p4est_vtk.h>
#include <p4est_bits.h>
#include <p4est_extended.h>
#include <p4est_iterate.h>



#define FLD_COUNT 4

#define GAM         1.4
#define ROOT_LEN    1.0


#define _MAX_(X,Y) ((X)>(Y) ? (X) : (Y))


#define _MIN_(X,Y) ((X)<(Y) ? (X) : (Y))

typedef struct femdg_data
{
    double gp[4][2];
    double gpf[4][2][2];
    double u[3];
    double qx[3];
    double qy[3];
    double gJx;
    double gJy;
    double u_orig;
    double error;
    double value[2][4][2];
    int    is_h[4];

    double invMassM[3][3];

    double u_int[3];
    double qx_int[3];
    double qy_int[3];


} femdg_data_t;

typedef struct femdg_ctx
{
    double              max_err;            /**< maximum allowed global interpolation error */
    int                 refine_period;      /**< the number of time steps between mesh refinement */
    int                 repartition_period; /**< the number of time steps between repartitioning */
    int                 write_period;       /**< the number of time steps between writing vtk files */
    int                 min_level;
    int                 max_level;
} femdg_ctx_t;





#endif //P4EST_DG_HEAT_DATA_H
