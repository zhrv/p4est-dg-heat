//
// Created by appmath on 10.06.17.
//

#include "data.h"
#include "utils.h"
#include "integrals.h"
#include "output.h"
#include "adapt.h"


static void femdg_calc_exist_solution(p4est_iter_volume_info_t * info, void *user_data){
    p4est_t            *p4est = info->p4est;
    p4est_quadrant_t   *q = info->quad;
    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;
    double              dt = *((double *) user_data);
    double              c[3];
    double              pi = 4.*atan(1.);

    femdg_get_midpoint(p4est,info->treeid,q,c);

    data->u_orig = exp(-pi*pi*dt)*sin(c[0]*pi)*sin(c[1]*pi);
    data->error = fabs(data->u[0] - data->u_orig);
}



static void femdg_mult_inverse(p4est_iter_volume_info_t * info, void *user_data){
    p4est_quadrant_t   *q = info->quad;
    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;
    double              dt = *((double *) user_data);

    for (int i = 0; i < 3; i++) {
        double uu = 0.0;
        for (int j = 0; j < 3; j++) {
            uu += data->invMassM[i][j]*data->u_int[j];
        }
        data->u[i] += uu*dt;
    }

}

static void femdg_mult_inverse_q(p4est_iter_volume_info_t * info, void *user_data){
    p4est_quadrant_t   *q = info->quad;
    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;
    for (int i = 0; i < 3; i++) {
        double qq[2] = {0., 0.};
        for (int j = 0; j < 3; j++) {
            qq[0] += data->invMassM[i][j]*data->qx_int[j];
            qq[1] += data->invMassM[i][j]*data->qy_int[j];
        }
        data->qx[i] = qq[0];
        data->qy[i] = qq[1];
    }

}

static double femdg_get_timestep (p4est_t * p4est)
{
    return 1.e-8;
}

static void femdg_timestep_q(p4est_t * p4est)
{
    //считаем двойные и flux
    p4est_iterate (p4est,                            /* the forest */
                   NULL,                             /* the ghost layer */
                   NULL,                             /* the synchronized ghost data */
                   femdg_calc_double_integral_q,     /* callback to compute each quad's interior contribution to du/dt */
                   femdg_calc_flux_q,                /* callback to compute each quads' faces' contributions to du/du */
                   NULL);

    //умножение на обратную матрицу и рассчет новых значений
    p4est_iterate (p4est,                  /* the forest */
                   NULL,                   /* the ghost layer */
                   NULL,                   /* the synchronized ghost data */
                   femdg_mult_inverse_q,   /* callback to compute each quad's interior contribution to du/dt */
                   NULL,                   /* callback to compute each quads' faces' contributions to du/du */
                   NULL);

}


static void femdg_timestep_u(p4est_t * p4est)
{
    //считаем двойные и flux
    p4est_iterate (p4est,                            /* the forest */
                   NULL,/*ghost, */                  /* the ghost layer */
                   NULL,/* (void *) ghost_data, */   /* the synchronized ghost data */
                   femdg_calc_double_integral,       /* callback to compute each quad's interior contribution to du/dt */
                   femdg_calc_flux,                  /* callback to compute each quads' faces' contributions to du/du */
                   NULL);
}


static void femdg_timestep_new(p4est_t * p4est, double dt)
{
    p4est_iterate (p4est,                 /* the forest */
                   NULL,                  /* the ghost layer */
                   (void *) &dt,          /* the synchronized ghost data */
                   femdg_mult_inverse,    /* callback to compute each quad's interior contribution to du/dt */
                   NULL,                  /* callback to compute each quads' faces' contributions to du/du */
                   NULL);

}


/** Timestep the advection problem.
 *
 * Update the state, refine, repartition, and write the solution to file.
 *
 * \param [in,out] p4est the forest, whose state is updated
 * \param [in] time      the end time
 */
void femdg_timestep (p4est_t * p4est, double time)
{
    double              t = 0.;
    double              dt = 0.;
    int                 i;
//    femdg_data_t       *ghost_data;
    femdg_ctx_t        *ctx = (femdg_ctx_t *) p4est->user_pointer;
    int                 refine_period = ctx->refine_period;
    int                 write_period = ctx->write_period;

    dt = femdg_get_timestep(p4est);
    for (t = 0., i = 0; t < time; i++) {
        P4EST_GLOBAL_PRODUCTIONF ("STEP: %6d | TIME: %16.8e\n", i, t);

        /* refine */
//        if (!(i % refine_period)) {
//            if (i) {
//                femdg_adapt(p4est);
//            }
//            dt = femdg_get_timestep (p4est);
//        }


        /* write out solution */
        if (!(i % write_period)) {
            femdg_write_solution (p4est, i);
            P4EST_GLOBAL_PRODUCTIONF ("***************\n* File for %6d step is saved\n***************\n", i);
        }

        //обнуляем интегралы
        p4est_iterate (p4est,                           /* the forest */
                       NULL,/*ghost, */                 /* the ghost layer */
                       NULL,/* (void *) ghost_data,*/   /* the synchronized ghost data */
                       femdg_zero_integrals,            /* callback to compute each quad's interior contribution to du/dt */
                       NULL,                            /* callback to compute each quads' faces' contributions to du/du */
                       NULL);

        femdg_timestep_q(p4est);
        femdg_timestep_u(p4est);
        femdg_timestep_new(p4est, dt);

        t += dt;

        //считаем точное значение
        p4est_iterate (p4est,                       /* the forest */
                       NULL,                        /* the ghost layer */
                       (void *) &t,                 /* the synchronized ghost data */
                       femdg_calc_exist_solution,   /* callback to compute each quad's interior contribution to du/dt */
                       NULL,                        /* callback to compute each quads' faces' contributions to du/du */
                       NULL);


    }

}
