//
// Created by appmath on 10.06.17.
//

#ifndef P4EST_DG_HEAT_INIT_H
#define P4EST_DG_HEAT_INIT_H

#include "data.h"

extern p4est_t * femdg_init(int *argc, char*** argv);


#endif //P4EST_DG_HEAT_INIT_H
