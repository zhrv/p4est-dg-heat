//
// Created by appmath on 10.06.17.
//
#include "data.h"
#include "utils.h"
#include "connectivity.h"
#include "output.h"
#include "adapt.h"

void femdg_init_initial_condition (p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * q)
{
    femdg_data_t       *data = (femdg_data_t *) q->p.user_data;
//    double              h = ROOT_LEN *
//                            (double) P4EST_QUADRANT_LEN (q->level) / (double) P4EST_ROOT_LEN;
    double              midpoint[3];
    double              pi = 4.*atan(1.);

    femdg_get_midpoint (p4est, which_tree, q, midpoint);


    data->u[0] = sin((midpoint[0])*pi)*sin((midpoint[1])*pi);
    data->u[1]=0.0;
    data->u[2]=0.0;

    data->u_orig = sin((midpoint[0])*pi)*sin((midpoint[1])*pi);
    data->error = 0.;


    femdg_calc_quad_gp(p4est, which_tree, q);
    femdg_calc_quad_mass_matr(p4est, which_tree, q);
}

femdg_ctx_t           ctx;


p4est_t * femdg_init(int *argc, char*** argv)
{
    p4est_t              *p4est;
    int                   mpiret;
    p4est_connectivity_t *conn;
    int                   recursive, partforcoarsen;
    sc_MPI_Comm           mpicomm;

    mpiret = sc_MPI_Init (argc, argv);
    SC_CHECK_MPI (mpiret);
    mpicomm = sc_MPI_COMM_WORLD;

    sc_init (mpicomm, 1, 1, NULL, SC_LP_ESSENTIAL);
    p4est_init (NULL, SC_LP_PRODUCTION);

    ctx.max_err             = 1.e-3;
    ctx.refine_period       = 2;
    ctx.repartition_period  = 4;
    ctx.write_period        = 1;
    ctx.min_level           = 4;
    ctx.max_level           = 5;

    conn = femdg_conn_create();

    p4est = p4est_new_ext (mpicomm,                       /* communicator */
                           conn,                          /* connectivity */
                           0,                             /* minimum quadrants per MPI process */
                           ctx.min_level,                 /* minimum level of refinement */
                           1,                             /* fill uniform */
                           sizeof (femdg_data_t),         /* data size */
                           femdg_init_initial_condition,  /* initializes data */
                           (void *) (&ctx));              /* context */

//    recursive = 1;
//    p4est_refine(p4est, recursive, femdg_refine_init_err_estimate, femdg_init_initial_condition);
//    p4est_coarsen(p4est, recursive, femdg_coarsen_init_err_estimate, femdg_init_initial_condition);
//
//    partforcoarsen = 1;
//    p4est_balance(p4est, P4EST_CONNECT_FACE, femdg_init_initial_condition);
//    p4est_partition(p4est, partforcoarsen, NULL);

    femdg_write_solution(p4est, 0);

    return p4est;
}

