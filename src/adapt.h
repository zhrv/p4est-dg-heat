//
// Created by appmath on 10.06.17.
//

#ifndef P4EST_DG_HEAT_ADAPT_H
#define P4EST_DG_HEAT_ADAPT_H
#include "data.h"

extern void femdg_adapt(p4est_t * p4est);
extern double femdg_error_sqr_estimate (p4est_quadrant_t * q);
extern int femdg_refine_init_err_estimate (p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * q);
extern int femdg_refine_err_estimate (p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * q);
extern int femdg_coarsen_init_err_estimate (p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * children[]);
extern int femdg_coarsen_err_estimate (p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * children[]);
extern void femdg_replace_quads (p4est_t * p4est, p4est_topidx_t which_tree,
                          int num_outgoing, p4est_quadrant_t * outgoing[],
                          int num_incoming, p4est_quadrant_t * incoming[]);



#endif //P4EST_DG_HEAT_ADAPT_H
