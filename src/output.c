//
// Created by appmath on 10.06.17.
//
#include "data.h"


static void femdg_interpolate_solution (p4est_iter_volume_info_t * info, void *user_data) {
    sc_array_t         **u_interp = (sc_array_t **) user_data;      /* we passed the array of values to fill as the user_data in the call to p4est_iterate */
    p4est_t            *p4est = info->p4est;
    p4est_quadrant_t   *q = info->quad;
    p4est_topidx_t      which_tree = info->treeid;
    p4est_locidx_t      local_id = info->quadid;  /* this is the index of q *within its tree's numbering*.  We want to convert it its index for all the quadrants on this process, which we do below */
    p4est_tree_t       *tree;
    femdg_data_t         *data = (femdg_data_t *) q->p.user_data;
//    double              h;
    p4est_locidx_t      arrayoffset;
//    double              this_u;
    double             *this_u_ptr;
//    int                 i, j;

    tree = p4est_tree_array_index (p4est->trees, which_tree);
    local_id += tree->quadrants_offset;   /* now the id is relative to the MPI process */
    arrayoffset = local_id;      /* each local quadrant has 2^d (P4EST_CHILDREN) values in u_interp */

    this_u_ptr = (double *) sc_array_index (u_interp[0], arrayoffset);
    this_u_ptr[0] = data->u[0];

    this_u_ptr = (double *) sc_array_index (u_interp[1], arrayoffset);
    this_u_ptr[0] = data->u[1];

    this_u_ptr = (double *) sc_array_index (u_interp[2], arrayoffset);
    this_u_ptr[0] = data->u[2];

    this_u_ptr = (double *) sc_array_index (u_interp[3], arrayoffset);
    this_u_ptr[0] = data->error;

    this_u_ptr = (double *) sc_array_index (u_interp[4], arrayoffset);
    this_u_ptr[0] = data->u_orig;

}

void femdg_write_solution (p4est_t * p4est, int timestep)
{
    char filename[BUFSIZ] = {'\0'};
    sc_array_t **u_interp;
    p4est_locidx_t numquads;

    snprintf(filename, 17, "result_%04d", timestep);

    numquads = p4est->local_num_quadrants;

    u_interp = (sc_array_t **) malloc(5 * sizeof(sc_array_t *));
    for (int i = 0; i < 5; i++) {
        u_interp[i] = sc_array_new_size(sizeof(double), numquads);
    }

    p4est_iterate(p4est, NULL,   /* we don't need any ghost quadrants for this loop */
                  (void *) u_interp,     /* pass in u_interp so that we can fill it */
                  femdg_interpolate_solution,    /* callback function that interpolates from the cell center to the cell corners, defined above */
                  NULL,          /* there is no callback for the faces between quadrants */
#ifdef P4_TO_P8
            NULL,          /* there is no callback for the edges between quadrants */
#endif
                  NULL);         /* there is no callback for the corners between quadrants */

    /* create VTK output context and set its parameters */
    p4est_vtk_context_t *context = p4est_vtk_context_new(p4est, filename);
    p4est_vtk_context_set_scale(context, 0.99999);  /* quadrant at almost full scale */

    /* begin writing the output files */
    context = p4est_vtk_write_header(context);
    SC_CHECK_ABORT(context != NULL, P4EST_STRING"_vtk: Error writing vtk header");

    context = p4est_vtk_write_cell_dataf(context, 0, 1,  /* do write the refinement level of each quadrant */
                                         1,      /* do write the mpi process id of each quadrant */
                                         1,      /* do not wrap the mpi rank (if this were > 0, the modulus of the rank relative to this number would be written instead of the rank) */
                                         5,      /* there is no custom cell scalar data. */
                                         0,      /* there is no custom cell vector data. */
                                         "u[0]", u_interp[0],
                                         "u[1]", u_interp[1],
                                         "u[2]", u_interp[2],
                                         "u_orig", u_interp[4],
                                         "error", u_interp[3],
                                         context);       /* mark the end of the variable cell data. */
    SC_CHECK_ABORT(context != NULL, P4EST_STRING"_vtk: Error writing cell data");

    const int retval = p4est_vtk_write_footer(context);
    SC_CHECK_ABORT(!retval, P4EST_STRING"_vtk: Error writing footer");

    for (int i = 0; i < 4; i++) {
        sc_array_destroy(u_interp[i]);
    }
    free(u_interp);
}


